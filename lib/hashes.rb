# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  hash = {}
  arr = str.split
  arr.each_with_index {|word, i|
    hash[word] = arr[i].length
  }
  hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  arr = hash.sort_by {|k, v| v}.to_a
  arr.last[0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each_key { |k|
    older[k] = newer[k]
  }
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  counters = Hash.new(0)
  letters = word.chars
  letters.each { |ch|
    counters[ch] += 1
  }
  counters
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  counter = Hash.new(0)
  arr.each { |el|
    counter[el] += 1
  }
  counter.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  counts = {:even => 0, :odd => 0}
  numbers.each {|num|
    if num%2 == 0
      counts[:even] += 1
    else
      counts[:odd] += 1
    end
  }
  counts
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = ["a", "e", "i", "o", "u"]
  vowel_arr = string.chars.select { |ch| vowels.include? (ch)}
  counter = Hash.new(0)
  vowel_arr.each {|vow|
    counter[vow] += 1
  }
  ordered = counter.sort_by {|k, v| [v, -k.ord]}
  ordered.last[0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students_with_birthdays)
  studs = students_with_birthdays.select {|k,v|
    v > 6}
  names = studs.keys
  res = []
  i = 0
  while i < names.length-1
    j = i + 1
    while j < names.length
      res.push([names[i], names[j]])
      j += 1
    end
    i += 1
  end
  res
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  counter = Hash.new(0)
  specimens.each {|anim|
    counter[anim] += 1}
  ordered = counter.sort_by {|k,v| v}
  ratio = ordered.first[1]/ordered.last[1]
  ordered.length**2 * ratio
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  norm = character_count(normal_sign)
  vand = character_count(vandalized_sign)
  vand.each {|k, v|
    if !norm.include?(k) || v > norm[k]
      return false
    end
  }
  true
end

def character_count(str)
  counter = Hash.new(0)
  new = str.downcase.chars.delete("!.?; @#$%^&*()-_=+></\|`~,:")
  new.each {|ch|
    counter[ch] += 1}
  counter
end
